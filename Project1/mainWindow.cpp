#include "mainWindow.h"

using namespace System;
using namespace System::Windows::Forms;

[STAThreadAttribute]
int main(array<String^> ^ args) {
	bks::readBooks();
	grs::readGenres();
	ats::readAuthors();
	crpt::readAzk();

	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	Project1::mainWindow form;
	Application::Run(%form);
	return 0;
}