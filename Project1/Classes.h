/*
*	genres.h
*
*	Created by Mateusz Urbanek on 10/16/2018.
*
*	2018 MATEUSZ URBANEK ( mateusz.urbanek.98@gmail.com )
*
*	GNU GPL v3.0
*
*/

#pragma once

#include <iostream>
/*
 *	Header that defines the standard input/output stream objects
 *	(source: http://www.cplusplus.com/reference/iostream/).
 *
 */

#ifndef MYCONFIG_H
#define MYCONFIG_H
#include "myConfig.h"
#endif

class Class1
{
public:
	Class1();
	~Class1();

	int ID_c1 = NULL;
	int DummyVar1 = 1;
	int DummyVar2 = 2;

	virtual operator int();
	virtual int size();

private:

protected:
};

Class1::Class1()
{
	if (DEBUG == true) std::cout << "Class1 constructor called" << std::endl;	//checking if constructor is called
}

Class1::~Class1()
{
	if (DEBUG == true) std::cout << "Class1 destructor called" << std::endl;	//checking if destructor is called
}

Class1::operator int()
{
	return size();
}

int Class1::size()
{
	int i = 0;
	i += sizeof(ID_c1);
	i += sizeof(DummyVar1);
	i += sizeof(DummyVar2);

	return i;
}

//----------------------------------------------------------------------------------------------------

class Class2
{
public:
	Class2();
	~Class2();

	int ID_c2 = NULL;
	int DummyVar1 = 1;
	int DummyVar2 = 2;

	virtual operator int();
	virtual int size();

private:

protected:
};

Class2::Class2()
{
	if (DEBUG == true) std::cout << "Class2 constructor called" << std::endl;	//checking if constructor is called
}

Class2::~Class2()
{
	if (DEBUG == true) std::cout << "Class2 destructor called" << std::endl;	//checking if destructor is called
}

Class2::operator int()
{
	return size();
}

int Class2::size()
{
	int i = 0;
	i += sizeof(ID_c2);
	i += sizeof(DummyVar1);
	i += sizeof(DummyVar2);

	return i;
}

//----------------------------------------------------------------------------------------------------

class myClass: public Class1, Class2
{
public:
	myClass();
	~myClass();

	int ID = NULL;

	virtual operator int();
	virtual int size();

private:

protected:
};

myClass::myClass()
{
	if (DEBUG == true) std::cout << "myClass constructor called" << std::endl;	//checking if constructor is called
}

myClass::~myClass()
{
	if (DEBUG == true) std::cout << "myClass destructor called" << std::endl;	//checking if destructor is called
}

myClass::operator int()
{
	return size();
}

int myClass::size()
{
	int i = 0;
	i += sizeof(ID);
	i += sizeof(ID_c1);
	i += sizeof(ID_c2);
	i += sizeof(Class1::DummyVar1);
	i += sizeof(Class1::DummyVar2);
	i += sizeof(Class2::DummyVar1);
	i += sizeof(Class2::DummyVar2);

	return i;
}