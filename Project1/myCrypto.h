/*
*	myCrypto.h
*
*	Created by Mateusz Urbanek on 10/16/2018.
*
*	2018 MATEUSZ URBANEK ( mateusz.urbanek.98@gmail.com )
*
*	GNU GPL v3.0
*
*/

#pragma once

#include <string>
/*
 *	This header introduces string types, character traits and a set of converting functions
 *	(source: http://www.cplusplus.com/reference/string/).
 *
 */

#include <fstream>
 /*
  *	Header providing file stream classes
  *	(source: http://www.cplusplus.com/reference/fstream/).
  *
  */

#include <sstream>
  /*
   *	Header providing string stream classes
   *	(source: http://www.cplusplus.com/reference/sstream/).
   *
   */

#include <vector>
   /*
	*	Header that defines the vector container class
	*	(source: http://www.cplusplus.com/reference/vector/).
	*
	*/

#include "fileLocations.h"

#ifndef MYCONFIG_H
#define MYCONFIG_H
#include "myConfig.h"
#endif

namespace crpt
{
	std::fstream f_azk;
	std::string fileInput;

	std::string decryption(std::string str_to_decr, std::string key);
	std::string encryption(std::string str_to_encr, std::string key);

	class Azk
	{
	public:
		Azk(std::string msg);
		~Azk();
		std::string azku;

	private:

	protected:
	};

	void readAzk();

	int _i_filesize;
	std::vector<Azk> arr_azk;
}

crpt::Azk::Azk(std::string smth)
{
	if (DEBUG == true) std::cout << "Azk constructor called" << std::endl;	//checking if constructor is called

	azku = smth;

	if (DEBUG == true)
	{
		std::cout << "Azku added \n"
			<< "Azku added:.........." << azku << std::endl;
	}
}

crpt::Azk::~Azk()
{
	if (DEBUG == true) std::cout << "Azk destructor called" << std::endl;	//checking if destructor is called
}

std::string crpt::decryption(std::string str_to_decr, std::string key)
{
	return encryption(str_to_decr, key);
}

std::string crpt::encryption(std::string str_to_encr, std::string key)
{
	std::string tmp(key);

	while (key.size() < str_to_encr.size())
		key += tmp;

	for (std::string::size_type i = 0; i < str_to_encr.size(); ++i)
		str_to_encr[i] ^= key[i];

	return str_to_encr;
}

void crpt::readAzk()
{
	crpt::f_azk.open(fls::azkFileLocation, std::ios::in);

	try
	{
		if (crpt::f_azk.good())
		{
			if (DEBUG == true) std::cout << "azkudagamlah.dat opened\n" << std::endl;

			std::string _s_filesize;
			_i_filesize;
			std::getline(crpt::f_azk, _s_filesize, '#');
			std::stringstream sti0(_s_filesize);
			sti0 >> _i_filesize;

			for (int i = 0; i < _i_filesize; i++)
			{
				crpt::Azk *newAzk;

				std::string _s_azk;

				std::getline(crpt::f_azk, _s_azk, '#');

				newAzk = new crpt::Azk(_s_azk);

				if (DEBUG == true) std::cout << newAzk << "\n" << std::endl;

				arr_azk.push_back(*newAzk);
			}
		}

		else throw myException();
	}
	catch (std::exception& e)
	{
		std::cout << "azkudagamlah.dat opened\n" << std::endl;
		std::cout << "File error opening file - file corrupted" << std::endl;
		crpt::f_azk.close(); // Closing file
		exit(1);
	}

	crpt::f_azk.close(); // Closing file
}