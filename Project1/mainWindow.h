#pragma once

#ifndef AUTHROS_H
#define AUTHORS_H
#include "Authors.h"
#endif // !AUTHROS_H

#ifndef BOOKS_H
#define BOOKS_H
#include "Books.h"
#endif // !BOOKS_H

#ifndef GENRES_H
#define GENRES_H
#include "Genres.h"
#endif // !GENRES_H

#ifndef MYCRYPTO_H
#define MYCRYPTO_H
#include "myCrypto.h"
#endif // !MYCRYPTO_H

#ifndef ACCWINDOW_H
#define ACCWINDOW_H
#include "accWindow.h"
#endif // !ACCWINDOW_H

#include <windows.h>

#ifndef MYCONFIG_H
#define MYCONFIG_H
#include "myConfig.h"
#endif

#include <msclr\marshal_cppstd.h>

namespace Project1 {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for mainWindow
	/// </summary>
	public ref class mainWindow : public System::Windows::Forms::Form
	{
	public:
		mainWindow(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~mainWindow()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::TextBox^  ID_Textbox;
	private: System::Windows::Forms::TextBox^  Name_Box;
	private: System::Windows::Forms::TextBox^  Surname_Box;
	private: System::Windows::Forms::TextBox^  statusTextBox;

	private: System::Windows::Forms::TextBox^  textBox5;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  Title_Box;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  Genre_Box;
	private: System::Windows::Forms::TextBox^  loginTextBox;
	private: System::Windows::Forms::MaskedTextBox^  PasswdTextBox;
	private: System::Windows::Forms::Button^  loginButton;

	private: System::Windows::Forms::Button^  signInButton;
	private: System::Windows::Forms::Button^  addEntryButton;

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->ID_Textbox = (gcnew System::Windows::Forms::TextBox());
			this->Name_Box = (gcnew System::Windows::Forms::TextBox());
			this->Surname_Box = (gcnew System::Windows::Forms::TextBox());
			this->statusTextBox = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->Title_Box = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->Genre_Box = (gcnew System::Windows::Forms::TextBox());
			this->loginTextBox = (gcnew System::Windows::Forms::TextBox());
			this->PasswdTextBox = (gcnew System::Windows::Forms::MaskedTextBox());
			this->loginButton = (gcnew System::Windows::Forms::Button());
			this->signInButton = (gcnew System::Windows::Forms::Button());
			this->addEntryButton = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			//
			// button1
			//
			this->button1->Enabled = false;
			this->button1->Location = System::Drawing::Point(455, 152);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(115, 27);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Next";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &mainWindow::button1_Click);
			//
			// button2
			//
			this->button2->Enabled = false;
			this->button2->Location = System::Drawing::Point(97, 152);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(115, 27);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Previous";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &mainWindow::button2_Click);
			//
			// button3
			//
			this->button3->Enabled = false;
			this->button3->Location = System::Drawing::Point(663, 12);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(86, 24);
			this->button3->TabIndex = 3;
			this->button3->Text = L"Authors";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &mainWindow::button3_Click);
			//
			// button4
			//
			this->button4->Enabled = false;
			this->button4->Location = System::Drawing::Point(663, 40);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(86, 24);
			this->button4->TabIndex = 4;
			this->button4->Text = L"Books";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &mainWindow::button4_Click);
			//
			// button5
			//
			this->button5->Enabled = false;
			this->button5->Location = System::Drawing::Point(663, 68);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(86, 24);
			this->button5->TabIndex = 5;
			this->button5->Text = L"Genres";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &mainWindow::button5_Click);
			//
			// ID_Textbox
			//
			this->ID_Textbox->Location = System::Drawing::Point(97, 12);
			this->ID_Textbox->Name = L"ID_Textbox";
			this->ID_Textbox->ReadOnly = true;
			this->ID_Textbox->Size = System::Drawing::Size(473, 22);
			this->ID_Textbox->TabIndex = 7;
			//
			// Name_Box
			//
			this->Name_Box->Location = System::Drawing::Point(97, 40);
			this->Name_Box->Name = L"Name_Box";
			this->Name_Box->ReadOnly = true;
			this->Name_Box->Size = System::Drawing::Size(473, 22);
			this->Name_Box->TabIndex = 8;
			//
			// Surname_Box
			//
			this->Surname_Box->Location = System::Drawing::Point(97, 68);
			this->Surname_Box->Name = L"Surname_Box";
			this->Surname_Box->ReadOnly = true;
			this->Surname_Box->Size = System::Drawing::Size(473, 22);
			this->Surname_Box->TabIndex = 9;
			//
			// statusTextBox
			//
			this->statusTextBox->Location = System::Drawing::Point(663, 124);
			this->statusTextBox->Name = L"statusTextBox";
			this->statusTextBox->ReadOnly = true;
			this->statusTextBox->Size = System::Drawing::Size(84, 22);
			this->statusTextBox->TabIndex = 10;
			//
			// textBox5
			//
			this->textBox5->Location = System::Drawing::Point(663, 96);
			this->textBox5->Name = L"textBox5";
			this->textBox5->ReadOnly = true;
			this->textBox5->Size = System::Drawing::Size(84, 22);
			this->textBox5->TabIndex = 11;
			//
			// label1
			//
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(9, 12);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(21, 17);
			this->label1->TabIndex = 12;
			this->label1->Text = L"ID";
			//
			// label2
			//
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(9, 40);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(45, 17);
			this->label2->TabIndex = 13;
			this->label2->Text = L"Name";
			//
			// label3
			//
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(9, 68);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(65, 17);
			this->label3->TabIndex = 14;
			this->label3->Text = L"Surname";
			//
			// label4
			//
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(9, 96);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(35, 17);
			this->label4->TabIndex = 16;
			this->label4->Text = L"Title";
			//
			// Title_Box
			//
			this->Title_Box->Location = System::Drawing::Point(97, 96);
			this->Title_Box->Name = L"Title_Box";
			this->Title_Box->ReadOnly = true;
			this->Title_Box->Size = System::Drawing::Size(473, 22);
			this->Title_Box->TabIndex = 15;
			//
			// label5
			//
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(9, 124);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(48, 17);
			this->label5->TabIndex = 17;
			this->label5->Text = L"Genre";
			//
			// Genre_Box
			//
			this->Genre_Box->Location = System::Drawing::Point(97, 124);
			this->Genre_Box->Name = L"Genre_Box";
			this->Genre_Box->ReadOnly = true;
			this->Genre_Box->Size = System::Drawing::Size(473, 22);
			this->Genre_Box->TabIndex = 18;
			//
			// loginTextBox
			//
			this->loginTextBox->Location = System::Drawing::Point(564, 308);
			this->loginTextBox->Name = L"loginTextBox";
			this->loginTextBox->Size = System::Drawing::Size(185, 22);
			this->loginTextBox->TabIndex = 19;
			if (DEBUG) this->loginTextBox->Text = L"admin";
			if (!DEBUG) this->loginTextBox->Text = L"login";
			//
			// PasswdTextBox
			//
			this->PasswdTextBox->Location = System::Drawing::Point(564, 336);
			this->PasswdTextBox->Name = L"PasswdTextBox";
			this->PasswdTextBox->PasswordChar = '*';
			this->PasswdTextBox->Size = System::Drawing::Size(185, 22);
			this->PasswdTextBox->TabIndex = 20;
			if (DEBUG) this->PasswdTextBox->Text = L"12345";
			if (!DEBUG) this->PasswdTextBox->Text = L"password";
			//
			// loginButton
			//
			this->loginButton->Location = System::Drawing::Point(564, 364);
			this->loginButton->Name = L"loginButton";
			this->loginButton->Size = System::Drawing::Size(102, 34);
			this->loginButton->TabIndex = 21;
			this->loginButton->Text = L"Login";
			this->loginButton->UseVisualStyleBackColor = true;
			this->loginButton->Click += gcnew System::EventHandler(this, &mainWindow::loginButton_Click);
			//
			// signInButton
			//
			this->signInButton->Location = System::Drawing::Point(672, 364);
			this->signInButton->Name = L"signInButton";
			this->signInButton->Size = System::Drawing::Size(77, 34);
			this->signInButton->TabIndex = 23;
			this->signInButton->Text = L"Sign in";
			this->signInButton->UseVisualStyleBackColor = true;
			this->signInButton->Click += gcnew System::EventHandler(this, &mainWindow::signInButton_Click);
			//
			// addEntryButton
			//
			this->addEntryButton->Enabled = false;
			this->addEntryButton->Location = System::Drawing::Point(12, 371);
			this->addEntryButton->Name = L"addEntryButton";
			this->addEntryButton->Size = System::Drawing::Size(165, 27);
			this->addEntryButton->TabIndex = 25;
			this->addEntryButton->Text = L"Add new entry";
			this->addEntryButton->UseVisualStyleBackColor = true;
			this->addEntryButton->Click += gcnew System::EventHandler(this, &mainWindow::addEntryButton_Click);
			//
			// mainWindow
			//
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(761, 410);
			this->Controls->Add(this->addEntryButton);
			this->Controls->Add(this->signInButton);
			this->Controls->Add(this->loginButton);
			this->Controls->Add(this->PasswdTextBox);
			this->Controls->Add(this->loginTextBox);
			this->Controls->Add(this->Genre_Box);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->Title_Box);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBox5);
			this->Controls->Add(this->statusTextBox);
			this->Controls->Add(this->Surname_Box);
			this->Controls->Add(this->Name_Box);
			this->Controls->Add(this->ID_Textbox);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Name = L"mainWindow";
			this->Text = L"Project";
			this->ResumeLayout(false);
			this->PerformLayout();
		}
#pragma endregion
		//----------------------------------------------------------------------------------------------------------------------
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		ID_Textbox->Text = "";
		Name_Box->Text = "";
		Surname_Box->Text = "";
		Title_Box->Text = "";
		Genre_Box->Text = "";

		if (cfg::which == 3)
		{
			if (cfg::what < ats::arr_authors.size() - 1)
			{
				cfg::what++;
			}

			ID_Textbox->Text = ats::arr_authors[cfg::what].author_ID.ToString();

			String^ author_name = gcnew String(ats::arr_authors[cfg::what].author_name.c_str());
			Name_Box->Text = author_name;

			String^ author_surname = gcnew String(ats::arr_authors[cfg::what].author_surname.c_str());
			Surname_Box->Text = author_surname;
		}

		if (cfg::which == 4)
		{
			if (cfg::what < bks::arr_books.size() - 1)
			{
				cfg::what++;
			}

			ID_Textbox->Text = bks::arr_books[cfg::what].book_ID.ToString();

			String^ title = gcnew String(bks::arr_books[cfg::what].title.c_str());
			Title_Box->Text = title;

			String^ author_name = gcnew String(ats::arr_authors[bks::arr_books[cfg::what].author_ID].author_name.c_str());
			Name_Box->Text = author_name;

			String^ author_surname = gcnew String(ats::arr_authors[bks::arr_books[cfg::what].author_ID].author_surname.c_str());
			Surname_Box->Text = author_surname;

			String^ genre = gcnew String(grs::arr_genres[bks::arr_books[cfg::what].author_ID].genre_name.c_str());
			Genre_Box->Text = genre;
		}

		if (cfg::which == 5)
		{
			if (cfg::what < grs::arr_genres.size() - 1)
			{
				cfg::what++;
			}

			String^ genre = gcnew String(grs::arr_genres[cfg::what].genre_name.c_str());
			Genre_Box->Text = genre;

			ID_Textbox->Text = grs::arr_genres[cfg::what].genre_ID.ToString();
		}

		textBox5->Text = cfg::what.ToString();
	}
			 //----------------------------------------------------------------------------------------------------------------------
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		ID_Textbox->Text = "";
		Name_Box->Text = "";
		Surname_Box->Text = "";
		Title_Box->Text = "";
		Genre_Box->Text = "";

		if (cfg::which == 3)
		{
			if (cfg::what > 0)
			{
				cfg::what--;
			}

			ID_Textbox->Text = ats::arr_authors[cfg::what].author_ID.ToString();

			String^ author_name = gcnew String(ats::arr_authors[cfg::what].author_name.c_str());
			Name_Box->Text = author_name;

			String^ author_surname = gcnew String(ats::arr_authors[cfg::what].author_surname.c_str());
			Surname_Box->Text = author_surname;
		}

		if (cfg::which == 4)
		{
			if (cfg::what > 0)
			{
				cfg::what--;
			}

			ID_Textbox->Text = bks::arr_books[cfg::what].book_ID.ToString();

			String^ title = gcnew String(bks::arr_books[cfg::what].title.c_str());
			Title_Box->Text = title;

			String^ author_name = gcnew String(ats::arr_authors[bks::arr_books[cfg::what].author_ID].author_name.c_str());
			Name_Box->Text = author_name;

			String^ author_surname = gcnew String(ats::arr_authors[bks::arr_books[cfg::what].author_ID].author_surname.c_str());
			Surname_Box->Text = author_surname;

			String^ genre = gcnew String(grs::arr_genres[bks::arr_books[cfg::what].genere_ID].genre_name.c_str());
			Genre_Box->Text = genre;
		}

		if (cfg::which == 5)
		{
			if (cfg::what > 0)
			{
				cfg::what--;
			}

			ID_Textbox->Text = grs::arr_genres[cfg::what].genre_ID.ToString();

			String^ genre = gcnew String(grs::arr_genres[cfg::what].genre_name.c_str());
			Genre_Box->Text = genre;
		}

		textBox5->Text = cfg::what.ToString();
	}
			 //----------------------------------------------------------------------------------------------------------------------
	private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
		statusTextBox->Text = "ATS OK";
		ID_Textbox->Text = "";
		Name_Box->Text = "";
		Surname_Box->Text = "";
		Title_Box->Text = "";
		Genre_Box->Text = "";

		addEntryButton->Enabled = true;
		button1->Enabled = true;
		button2->Enabled = true;

		cfg::what = 0;
		cfg::which = 3;
		ID_Textbox->Text = ats::arr_authors[cfg::what].author_ID.ToString();
		textBox5->Text = cfg::what.ToString();

		String^ author_name = gcnew String(ats::arr_authors[cfg::what].author_name.c_str());
		Name_Box->Text = author_name;

		String^ author_surname = gcnew String(ats::arr_authors[cfg::what].author_surname.c_str());
		Surname_Box->Text = author_surname;
	}
			 //----------------------------------------------------------------------------------------------------------------------
	private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
		statusTextBox->Text = "BKS OK";
		ID_Textbox->Text = "";
		Name_Box->Text = "";
		Surname_Box->Text = "";
		Title_Box->Text = "";
		Genre_Box->Text = "";

		addEntryButton->Enabled = true;
		button1->Enabled = true;
		button2->Enabled = true;

		cfg::what = 0;
		cfg::which = 4;

		ID_Textbox->Text = bks::arr_books[cfg::what].book_ID.ToString();
		textBox5->Text = cfg::what.ToString();

		String^ title = gcnew String(bks::arr_books[cfg::what].title.c_str());
		Title_Box->Text = title;

		String^ author_name = gcnew String(ats::arr_authors[bks::arr_books[cfg::what].author_ID].author_name.c_str());
		Name_Box->Text = author_name;

		String^ author_surname = gcnew String(ats::arr_authors[bks::arr_books[cfg::what].author_ID].author_surname.c_str());
		Surname_Box->Text = author_surname;

		String^ genre = gcnew String(grs::arr_genres[bks::arr_books[cfg::what].genere_ID].genre_name.c_str());
		Genre_Box->Text = genre;
	}
			 //----------------------------------------------------------------------------------------------------------------------
	private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
		statusTextBox->Text = "GRS OK";
		ID_Textbox->Text = "";
		Name_Box->Text = "";
		Surname_Box->Text = "";
		Title_Box->Text = "";
		Genre_Box->Text = "";

		addEntryButton->Enabled = true;
		button1->Enabled = true;
		button2->Enabled = true;

		cfg::what = 0;
		cfg::which = 5;

		ID_Textbox->Text = grs::arr_genres[cfg::what].genre_ID.ToString();
		textBox5->Text = cfg::what.ToString();

		String^ genre = gcnew String(grs::arr_genres[cfg::what].genre_name.c_str());
		Genre_Box->Text = genre;
	}
			 //----------------------------------------------------------------------------------------------------------------------
	private: System::Void loginButton_Click(System::Object^  sender, System::EventArgs^  e) {
		crpt::readAzk();

		String^ key = PasswdTextBox->Text->ToString();
		std::string _key = msclr::interop::marshal_as<std::string>(key);

		String^ lgn = loginTextBox->Text->ToString();
		std::string _lgn = msclr::interop::marshal_as<std::string>(lgn);

		for (int i = 0; i < crpt::_i_filesize; i++)
		{
			if (_lgn == crpt::decryption(crpt::arr_azk[i].azku, _key))
			{
				cfg::login_state = 1;
				button3->Enabled = true;
				button4->Enabled = true;
				button5->Enabled = true;
				loginButton->Enabled = false;
				signInButton->Enabled = false;
				loginTextBox->ReadOnly = true;
				PasswdTextBox->ReadOnly = true;
				break;
			}
			else cfg::login_state = 0;
		}

		if (cfg::login_state == 1)
		{
			statusTextBox->Text = "Logged in";
		}
		else
		{
			auto result = MessageBox::Show("Wrong Login & Key combination!", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);

			if (result == System::Windows::Forms::DialogResult::OK) statusTextBox->Text = "Error";
		}
	}
			 //----------------------------------------------------------------------------------------------------------------------
	private: System::Void signInButton_Click(System::Object^  sender, System::EventArgs^  e) {
		accWindow^ acw = gcnew accWindow();

		acw->Show();

		statusTextBox->Text = "Opened";
	}
			 //----------------------------------------------------------------------------------------------------------------------
	private: System::Void addEntryButton_Click(System::Object^  sender, System::EventArgs^  e) {
		if (cfg::awdw == 0)
		{
			if (cfg::which == 3)
			{
				Name_Box->ReadOnly = false;
				Surname_Box->ReadOnly = false;
				button1->Enabled = false;
				button2->Enabled = false;
				button3->Enabled = false;
				button4->Enabled = false;
				button5->Enabled = false;
				cfg::awdw = 1;
				addEntryButton->Text = L"Add";
			}

			else if (cfg::which == 4)
			{
				Surname_Box->ReadOnly = false;
				label3->Text = L"Author ID";
				Title_Box->ReadOnly = false;
				Genre_Box->ReadOnly = false;
				label5->Text = L"Genre ID";
				button1->Enabled = false;
				button2->Enabled = false;
				button3->Enabled = false;
				button4->Enabled = false;
				button5->Enabled = false;
				cfg::awdw = 1;
				addEntryButton->Text = L"Add";
			}

			else if (cfg::which == 5)
			{
				Genre_Box->ReadOnly = false;
				button1->Enabled = false;
				button2->Enabled = false;
				button3->Enabled = false;
				button4->Enabled = false;
				button5->Enabled = false;
				cfg::awdw = 1;
				addEntryButton->Text = L"Add";
			}
		}
		else
		{
			Name_Box->ReadOnly = true;
			Surname_Box->ReadOnly = true;
			label3->Text = L"Surname";
			label5->Text = L"Genre";
			Title_Box->ReadOnly = true;
			Genre_Box->ReadOnly = true;
			button1->Enabled = true;
			button2->Enabled = true;
			button3->Enabled = true;
			button4->Enabled = true;
			button5->Enabled = true;
			cfg::awdw = 0;

			std::fstream file;
			std::ofstream _file;
			int _i_filesize;
			std::string _s_filesize;
			std::string content;

			if (cfg::which == 3)
			{
				String^ name = Name_Box->Text->ToString();
				std::string _name = msclr::interop::marshal_as<std::string>(name);
				String^ surname = Surname_Box->Text->ToString();
				std::string _surname = msclr::interop::marshal_as<std::string>(surname);

				file.open(fls::authorsFileLocation, std::ios::in);
				std::getline(file, _s_filesize, '#');
				std::stringstream sti0(_s_filesize);
				sti0 >> _i_filesize;
				getline(file, content);
				_i_filesize += 1;
				file.close();

				_file.open(fls::authorsFileLocation, std::ios::trunc);
				_file << _i_filesize << "#" << content << _i_filesize - 1 << "#" << _name << "#" << _surname << "#";
				_file.close();

				MessageBox::Show("New entry added!", "Succes", MessageBoxButtons::OK, MessageBoxIcon::Information);
				
				bks::arr_books.clear();
				grs::arr_genres.clear();
				ats::arr_authors.clear();
				bks::readBooks();
				grs::readGenres();
				ats::readAuthors();
			}

			else if (cfg::which == 4)
			{
				try
				{
					int number;
					if (!int::TryParse(Surname_Box->Text, number)) {
						throw 1;
					}
					if (!int::TryParse(Genre_Box->Text, number)) {
						throw 2;
					}
					else throw 0;
				}
				catch (int e)
				{
					if (e == 0)
					{
						String^ surname = Surname_Box->Text->ToString();
						int surname_ID = int::Parse(surname);
						String^ title = Title_Box->Text->ToString();
						std::string _title = msclr::interop::marshal_as<std::string>(title);
						String^ genre = Genre_Box->Text->ToString();
						int genre_ID = int::Parse(genre);

						file.open(fls::booksFileLocation, std::ios::in);
						std::getline(file, _s_filesize, '#');
						std::stringstream sti0(_s_filesize);
						sti0 >> _i_filesize;
						getline(file, content);
						_i_filesize += 1;
						file.close();

						_file.open(fls::booksFileLocation, std::ios::trunc);
						_file << _i_filesize << "#" << content << _i_filesize - 1 << "#" << _title << "#" << surname_ID << "#" << genre_ID << "#";
						_file.close();

						MessageBox::Show("New entry added!", "Succes", MessageBoxButtons::OK, MessageBoxIcon::Information);
						
						bks::arr_books.clear();
						grs::arr_genres.clear();
						ats::arr_authors.clear();
						bks::readBooks();
						grs::readGenres();
						ats::readAuthors();
					}

					else if (e == 1)
					{
						MessageBox::Show("Author ID is not an number!", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
					}

					else if (e == 2)
					{
						MessageBox::Show("Genre ID is not an number!", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
					}
				}
			}

			else if (cfg::which == 5)
			{
				String^ genre = Genre_Box->Text->ToString();
				std::string _genre = msclr::interop::marshal_as<std::string>(genre);

				file.open(fls::genresFileLocation, std::ios::in);
				std::getline(file, _s_filesize, '#');
				std::stringstream sti0(_s_filesize);
				sti0 >> _i_filesize;
				getline(file, content);
				_i_filesize += 1;
				file.close();

				_file.open(fls::genresFileLocation, std::ios::trunc);
				_file << _i_filesize << "#" << content << _i_filesize - 1 << "#" << _genre << "#";
				_file.close();

				MessageBox::Show("New entry added!", "Succes", MessageBoxButtons::OK, MessageBoxIcon::Information);
				
				bks::arr_books.clear();
				grs::arr_genres.clear();
				ats::arr_authors.clear();				
				bks::readBooks();
				grs::readGenres();
				ats::readAuthors();
			}

			addEntryButton->Text = L"Add new entry";
		}
	}
	};
}