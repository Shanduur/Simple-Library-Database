/*
*	genres.h
*
*	Created by Mateusz Urbanek on 10/16/2018.
*
*	2018 MATEUSZ URBANEK ( mateusz.urbanek.98@gmail.com )
*
*	GNU GPL v3.0
*
*/

#pragma once

#include <iostream>
/*
 *	Header that defines the standard input/output stream objects
 *	(source: http://www.cplusplus.com/reference/iostream/).
 *
 */

#include <string>
 /*
  *	This header introduces string types, character traits and a set of converting functions
  *	(source: http://www.cplusplus.com/reference/string/).
  *
  */

#include <fstream>
  /*
   *	Header providing file stream classes
   *	(source: http://www.cplusplus.com/reference/fstream/).
   *
   */

#include <vector>
   /*
	*	Header that defines the vector container class
	*	(source: http://www.cplusplus.com/reference/vector/).
	*
	*/

#include <sstream>
	/*
	 *	Header providing string stream classes
	 *	(source: http://www.cplusplus.com/reference/sstream/).
	 *
	 */

#include "fileLocations.h"
	 //#include "myInput.h"
#include "Classes.h"

namespace grs
{
	class genre : public myClass
	{
	public:
		genre(std::string _genre_name, int _genre_ID);
		~genre();
		int genre_ID;
		std::string genre_name;

		virtual operator int();
		virtual int size();

	private:

	protected:
	};

	std::vector<genre> arr_genres;

	std::fstream f_genres;
	std::string fileInput;

	void addGenre();
	void readGenres();
	void saveGenres();
	void deleteGenre();
}

grs::genre::operator int()
{
	return size();
}

int grs::genre::size()
{
	int i = 0;

	i += sizeof(genre_ID);
	i += sizeof(genre_name);

	return i;
}

grs::genre::genre(std::string _genre_name, int _genre_ID)
{
	if (DEBUG == true) std::cout << "Genres constructor called" << std::endl;	//checking if constructor is called

	genre_name = _genre_name;
	genre_ID = _genre_ID;

	if (DEBUG == true)
	{
		std::cout << "genre added \n"
			<< "genre:..............." << genre_name << "\n"
			<< "genre ID:............" << genre_ID << std::endl;
	}
}

grs::genre::~genre()
{
	if (DEBUG == true) std::cout << "Genres destructor called" << std::endl;	//checking if destructor is called
}

//void grs::addGenre()
//{
//	grs::genre *newGenre;
//
//	std::string _genre_name;
//	int _genre_ID;
//
//	std::cin >> _genre_name;
//	_genre_ID = myInp::cin();
//	myInp::exceptionHandler(_genre_ID);
//
//	newGenre = new grs::genre(_genre_name, _genre_ID);
//
//	std::cout << newGenre << "\n" << std::endl;
//
//	arr_genres.push_back(*newGenre);
//}

void grs::readGenres()
{
	grs::f_genres.open(fls::genresFileLocation, std::ios::in);

	try
	{
		if (grs::f_genres.good())
		{
			if (DEBUG == true) std::cout << "genres.dat opened\n" << std::endl;

			std::string _s_filesize;
			int _i_filesize;
			std::getline(grs::f_genres, _s_filesize, '#');
			std::stringstream sti0(_s_filesize);
			sti0 >> _i_filesize;

			for (int i = 0; i < _i_filesize; i++)
			{
				grs::genre *newGenre;

				std::string _s_genre_name;
				std::string _s_genre_ID;
				int _i_genre_ID;

				std::getline(grs::f_genres, _s_genre_ID, '#');
				std::getline(grs::f_genres, _s_genre_name, '#');

				std::stringstream sti1(_s_genre_ID);
				sti1 >> _i_genre_ID;

				newGenre = new grs::genre(_s_genre_name, _i_genre_ID);

				if (DEBUG == true) std::cout << newGenre << "\n" << std::endl;

				arr_genres.push_back(*newGenre);
			}
		}

		else throw myException();
	}
	catch (std::exception& e)
	{
		std::cout << "genres.dat opened\n" << std::endl;
		std::cout << "File error opening file - file corrupted" << std::endl;
		grs::f_genres.close(); // Closing file
		exit(1);
	}

	grs::f_genres.close(); // Closing file
}

void grs::saveGenres()
{
	grs::f_genres.open(fls::genresFileLocation, std::ios::trunc);

	if (DEBUG == true) std::cout << "genres.dat opened\n" << std::endl;

	grs::f_genres << arr_genres.size() << "#";

	for (int i = 0; i <= arr_genres.size(); i++)
	{
		grs::f_genres << arr_genres[i].genre_ID << "#"
			<< arr_genres[i].genre_name << "#";
	}

	grs::f_genres.close(); // Closing file
}

void grs::deleteGenre()
{
	//TODO
}