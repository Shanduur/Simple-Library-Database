/*
*	fileLocations.h
*
*	Created by Mateusz Urbanek on 10/16/2018.
*
*	2018 MATEUSZ URBANEK ( mateusz.urbanek.98@gmail.com )
*
*	GNU GPL v3.0
*
*/

#pragma once

#include <string>
/*
 *	This header introduces string types, character traits and a set of converting functions
 *	(source: http://www.cplusplus.com/reference/string/).
 *
 */

#ifndef MYCONFIG_H
#define MYCONFIG_H
#include "myConfig.h"
#endif

namespace fls
{
	std::string authorsFileLocation = "authors.dat";
	std::string genresFileLocation = "genres.dat";
	std::string booksFileLocation = "books.dat";
	std::string usersFileLocation = "users.dat";
	std::string azkFileLocation = "azkudagamlah.dat";
}