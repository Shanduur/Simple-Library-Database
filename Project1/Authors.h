/*
*	Authors.h
*
*	Created by Mateusz Urbanek on 10/16/2018.
*
*	2018 MATEUSZ URBANEK ( mateusz.urbanek.98@gmail.com )
*
*	GNU GPL v3.0
*
*/

#pragma once

#include <iostream>
/*
 *	Header that defines the standard input/output stream objects
 *	(source: http://www.cplusplus.com/reference/iostream/).
 *
 */

#include <string>
 /*
  *	This header introduces string types, character traits and a set of converting functions
  *	(source: http://www.cplusplus.com/reference/string/).
  *
  */

#include <fstream>
  /*
   *	Header providing file stream classes
   *	(source: http://www.cplusplus.com/reference/fstream/).
   *
   */

#include <vector>
   /*
	*	Header that defines the vector container class
	*	(source: http://www.cplusplus.com/reference/vector/).
	*
	*/

#include <sstream>
	/*
	 *	Header providing string stream classes
	 *	(source: http://www.cplusplus.com/reference/sstream/).
	 *
	 */

#include "fileLocations.h"
	 //#include "myInput.h"
#include "Classes.h"

namespace ats
{
	class author : public myClass
	{
	public:
		author(int _author_ID, std::string _author_name, std::string _author_surname);
		~author();
		int author_ID;
		std::string author_name;
		std::string author_surname;

		virtual operator int();
		virtual int size();

	private:

	protected:
	};

	std::fstream f_authors;
	std::string fileInput;

	std::vector<author> arr_authors;

	void addAuthor();
	void readAuthors();
	void saveAuthors();
	void deleteAuthor();
}

ats::author::operator int()
{
	return size();
}

int ats::author::size()
{
	int i = 0;

	i += sizeof(author_ID);
	i += sizeof(author_name);
	i += sizeof(author_surname);

	return i;
}

ats::author::author(int _author_ID, std::string _author_name, std::string _author_surname)
{
	if (DEBUG == true) std::cout << "Author constructor called" << std::endl;	//checking if constructor is called

	author_name = _author_name;
	author_surname = _author_surname;
	author_ID = _author_ID;

	if (DEBUG == true)
	{
		std::cout << "Author added \n"
			<< "Author name:.........." << author_name << "\n"
			<< "Author surname:......." << author_surname << "\n"
			<< "Author ID:............" << author_ID << std::endl;
	}
}

ats::author::~author()
{
	if (DEBUG == true) std::cout << "Authors destructor called" << std::endl;	//checking if destructor is called
}

//void ats::addAuthor()
//{
//	ats::author *newAuthor;
//
//	std::string _author_name;
//	std::string _author_surname;
//	int _author_ID;
//
//	std::cin >> _author_name;
//	std::cin >> _author_surname;
//	//_author_ID = myInp::cin();
//	myInp::exceptionHandler(_author_ID);
//
//	newAuthor = new ats::author(_author_ID, _author_name, _author_surname);
//
//	std::cout << newAuthor << "\n" << std::endl;
//
//	arr_authors.push_back(*newAuthor);
//}

void ats::readAuthors()
{
	ats::f_authors.open(fls::authorsFileLocation, std::ios::in);

	try
	{
		if (ats::f_authors.good())
		{
			if (DEBUG == true) std::cout << "authors.dat opened\n" << std::endl;

			std::string _s_filesize;
			int _i_filesize;
			std::getline(ats::f_authors, _s_filesize, '#');
			std::stringstream sti0(_s_filesize);
			sti0 >> _i_filesize;

			for (int i = 0; i < _i_filesize; i++)
			{
				ats::author *newAuthor;

				std::string _s_author_name;
				std::string _s_author_surname;
				std::string _s_author_ID;
				int _i_author_ID;

				std::getline(ats::f_authors, _s_author_ID, '#');
				std::getline(ats::f_authors, _s_author_name, '#');
				std::getline(ats::f_authors, _s_author_surname, '#');

				std::stringstream sti1(_s_author_ID);
				sti1 >> _i_author_ID;

				newAuthor = new ats::author(_i_author_ID, _s_author_name, _s_author_surname);

				if (DEBUG == true) std::cout << newAuthor << "\n" << std::endl;

				arr_authors.push_back(*newAuthor);
			}
		}

		else throw myException();
	}
	catch (std::exception& e)
	{
		std::cout << "authors.dat opened\n" << std::endl;
		std::cout << "File error opening file - file corrupted" << std::endl;
		ats::f_authors.close(); // Closing file
		exit(1);
	}

	ats::f_authors.close(); // Closing file
}

void ats::saveAuthors()
{
	ats::f_authors.open(fls::genresFileLocation, std::ios::trunc);

	if (DEBUG == true) std::cout << "genres.dat opened\n" << std::endl;

	ats::f_authors << arr_authors.size() << "#";

	for (int i = 0; i <= arr_authors.size(); i++)
	{
		ats::f_authors << arr_authors[i].author_ID << "#"
			<< arr_authors[i].author_name << "#"
			<< arr_authors[i].author_surname << "#";
	}

	ats::f_authors.close(); // Closing file
}

void ats::deleteAuthor()
{
	//TODO
}