/*
*	myConfig.h
*
*	Created by Mateusz Urbanek on 10/16/2018.
*
*	2018 MATEUSZ URBANEK ( mateusz.urbanek.98@gmail.com )
*
*	GNU GPL v3.0
*
*/

#pragma once

#include <iostream>
/*
 *	Header that defines the standard input/output stream objects
 *	(source: http://www.cplusplus.com/reference/iostream/).
 *
 */

#include <exception>
 /*
  *	This header defines the base class for all exceptions thrown by
  *	the elements of the standard library: exception,
  *	along with several types and utilities to assist handling exceptions
  *	(source: http://www.cplusplus.com/reference/exception/).
  *
  */

#include <chrono>
  /*
   *	The elements in this header deal with time. This is done mainly by means of three concepts:
   *	* Durations
   *	* Time points
   *	* Clocks
   *	(source: http://www.cplusplus.com/reference/chrono/).
   *
   */

#include <windows.h>

#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS
#endif

#define _WIN32_WINNT 0x0500

bool DEBUG = true;

namespace cfg {
	int what = 0;
	int which = 0;
	int login_state = 0;
	int awdw = 0;
}

int8_t millis = 5000;

struct myException : public std::exception {
	const char * what() const throw () {
		return "Exception";
	}
};