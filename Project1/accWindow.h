#pragma once

#include <string>
#include <sstream>
#include <msclr\marshal_cppstd.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>

namespace Project1 {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for accWindow
	/// </summary>
	public ref class accWindow : public System::Windows::Forms::Form
	{
	public:
		accWindow(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~accWindow()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  Save;
	private: System::Windows::Forms::TextBox^  usernameTextBox;
	protected:

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->Save = (gcnew System::Windows::Forms::Button());
			this->usernameTextBox = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			//
			// Save
			//
			this->Save->Location = System::Drawing::Point(12, 57);
			this->Save->Name = L"Save";
			this->Save->Size = System::Drawing::Size(771, 30);
			this->Save->TabIndex = 0;
			this->Save->Text = L"Generate Password";
			this->Save->UseVisualStyleBackColor = true;
			this->Save->Click += gcnew System::EventHandler(this, &accWindow::Save_Click);
			//
			// usernameTextBox
			//
			this->usernameTextBox->Location = System::Drawing::Point(12, 29);
			this->usernameTextBox->Name = L"usernameTextBox";
			this->usernameTextBox->Size = System::Drawing::Size(771, 22);
			this->usernameTextBox->TabIndex = 1;
			//
			// label1
			//
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(9, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(155, 17);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Provide your username";
			//
			// label2
			//
			this->label2->AutoSize = true;
			this->label2->ForeColor = System::Drawing::Color::Red;
			this->label2->Location = System::Drawing::Point(417, 90);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(366, 17);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Note, that your key will be aviable at next run of program!";
			//
			// accWindow
			//
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(795, 114);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->usernameTextBox);
			this->Controls->Add(this->Save);
			this->Name = L"accWindow";
			this->Text = L"Account Creation";
			this->ResumeLayout(false);
			this->PerformLayout();
		}
#pragma endregion
	private: System::Void Save_Click(System::Object^  sender, System::EventArgs^  e) {
		String^ login = usernameTextBox->Text->ToString();
		std::string stdLogin = msclr::interop::marshal_as<std::string>(login);
		srand(time(NULL));

		std::fstream f_azk;
		std::ofstream _f_azk;

		try
		{
			if (login == "admin" || login == "Admin" || login == "ADMIN")
			{
				throw std::exception();
			}
			else
			{
				String^ key;
				for (int i = 0; i < stdLogin.length(); i++)
				{
					key += (rand() % 10).ToString();
				}

				std::string stdKey = msclr::interop::marshal_as<std::string>(key);
				std::string _s_filesize;
				int _i_filesize;
				std::string content;

				f_azk.open("azkudagamlah.dat", std::ios::app);
				f_azk << encryption(stdLogin, stdKey) << "#";
				f_azk.close();
				f_azk.open("azkudagamlah.dat", std::ios::in);
				std::getline(f_azk, _s_filesize, '#');
				std::stringstream sti0(_s_filesize);
				sti0 >> _i_filesize;
				getline(f_azk, content);
				_i_filesize += 1;
				f_azk.close();
				_f_azk.open("azkudagamlah.dat", std::ios::trunc);
				_f_azk << _i_filesize << "#" << content;
				_f_azk.close();

				MessageBox::Show("Key = " + key, "Your Key", MessageBoxButtons::OK, MessageBoxIcon::Information);
				MessageBox::Show("Note, that your key will be aviable at next run of program!", "Warning", MessageBoxButtons::OK, MessageBoxIcon::Warning);
			}
		}
		catch (std::exception& e)
		{
			MessageBox::Show("You have no permissions to use this username", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
		}
	}

	private: std::string encryption(std::string str_to_encr, std::string key)
	{
		std::string tmp(key);

		while (key.size() < str_to_encr.size())
			key += tmp;

		for (std::string::size_type i = 0; i < str_to_encr.size(); ++i)
			str_to_encr[i] ^= key[i];

		return str_to_encr;
	}
	};
}
