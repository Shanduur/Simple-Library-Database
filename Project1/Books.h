/*
*	Books.h
*
*	Created by Mateusz Urbanek on 10/16/2018.
*
*	2018 MATEUSZ URBANEK ( mateusz.urbanek.98@gmail.com )
*
*	GNU GPL v3.0
*
*/

#pragma once

#include <iostream>
/*
 *	Header that defines the standard input/output stream objects
 *	(source: http://www.cplusplus.com/reference/iostream/).
 *
 */

#include <string>
 /*
  *	This header introduces string types, character traits and a set of converting functions
  *	(source: http://www.cplusplus.com/reference/string/).
  *
  */

#include <fstream>
  /*
   *	Header providing file stream classes
   *	(source: http://www.cplusplus.com/reference/fstream/).
   *
   */

#include <vector>
   /*
	*	Header that defines the vector container class
	*	(source: http://www.cplusplus.com/reference/vector/).
	*
	*/

#include <sstream>
	/*
	 *	Header providing string stream classes
	 *	(source: http://www.cplusplus.com/reference/sstream/).
	 *
	 */

#include "fileLocations.h"
	 //#include "myInput.h"
#include "Classes.h"

namespace bks
{
	class book : public myClass
	{
	public:
		book(int _author_ID, std::string _title, int _genere_ID, int _book_ID);
		~book();
		int author_ID;
		int book_ID;
		int genere_ID;
		std::string title;

		virtual operator int();
		virtual int size();

	private:

	protected:
	};

	std::vector<book> arr_books;

	std::fstream f_books;
	std::string fileInput;

	void addBook();
	void readBooks();
	void saveBooks();
	void deleteBook();
}

bks::book::book(int _author_ID, std::string _title, int _genere_ID, int _book_ID)
{
	if (DEBUG == true) std::cout << "Book constructor called" << std::endl;	//checking if constructor is called

	title = _title;
	book_ID = _book_ID;
	genere_ID = _genere_ID;
	author_ID = _author_ID;

	if (DEBUG == true)
	{
		std::cout << "Book added \n"
			<< "Book ID:.............." << book_ID << "\n"
			<< "Title:................" << title << "\n"
			<< "Author:..............." << author_ID << "\n"
			<< "Genere:..............." << genere_ID << std::endl;
	}
}

bks::book::~book()
{
	if (DEBUG == true) std::cout << "Book destructor called" << std::endl;	//checking if destructor is called
}

bks::book::operator int()
{
	return size();
}

int bks::book::size()
{
	int i = 0;

	i += sizeof(author_ID);
	i += sizeof(genere_ID);
	i += sizeof(book_ID);
	i += sizeof(title);

	return i;
}

//void bks::addBook()
//{
//	bks::book *newBook;
//
//	std::string _title;
//	int _genere_ID;
//	int _author_ID;
//	int _book_ID;
//
//	std::cin >> _title;
//	//_genere_ID = myInp::cin();
//	//_author_ID = myInp::cin();
//	myInp::exceptionHandler(_genere_ID);
//	myInp::exceptionHandler(_author_ID);
//	myInp::exceptionHandler(_book_ID);
//
//	newBook = new bks::book(_author_ID, _title, _genere_ID, _book_ID);
//
//	std::cout << newBook << std::endl;
//
//	arr_books.push_back(*newBook);
//}

void bks::readBooks()
{
	bks::f_books.open(fls::booksFileLocation, std::ios::in);

	try
	{
		if (bks::f_books.good())
		{
			if (DEBUG == true) std::cout << "books.dat opened\n" << std::endl;

			std::string _s_filesize;
			int _i_filesize;
			std::getline(bks::f_books, _s_filesize, '#');
			std::stringstream sti0(_s_filesize);
			sti0 >> _i_filesize;

			for (int i = 0; i < _i_filesize; i++)
			{
				bks::book *newBook;

				std::string _s_title;
				std::string _s_genere_ID;
				std::string _s_author_ID;
				std::string _s_book_ID;
				int _i_genere_ID;
				int _i_author_ID;
				int _i_book_ID;

				std::getline(bks::f_books, _s_book_ID, '#');
				std::getline(bks::f_books, _s_title, '#');
				std::getline(bks::f_books, _s_genere_ID, '#');
				std::getline(bks::f_books, _s_author_ID, '#');

				std::stringstream sti1(_s_genere_ID);
				sti1 >> _i_genere_ID;
				std::stringstream sti2(_s_author_ID);
				sti2 >> _i_author_ID;
				std::stringstream sti3(_s_book_ID);
				sti3 >> _i_book_ID;

				newBook = new bks::book(_i_author_ID, _s_title, _i_genere_ID, _i_book_ID);

				if (DEBUG == true) std::cout << newBook << "\n" << std::endl;

				arr_books.push_back(*newBook);
			}
		}

		else throw myException();
	}
	catch (std::exception& e)
	{
		std::cout << "books.dat opened\n" << std::endl;
		std::cout << "File error opening file - file corrupted" << std::endl;
		bks::f_books.close(); // Closing file
		exit(1);
	}

	bks::f_books.close(); // Closing file
}

void bks::saveBooks()
{
	bks::f_books.open(fls::genresFileLocation, std::ios::trunc);

	if (DEBUG == true) std::cout << "genres.dat opened\n" << std::endl;

	bks::f_books << arr_books.size() << "#";

	for (int i = 0; i <= arr_books.size(); i++)
	{
		bks::f_books << arr_books[i].book_ID << "#"
			<< arr_books[i].title << "#"
			<< arr_books[i].genere_ID << "#"
			<< arr_books[i].author_ID << "#";
	}

	bks::f_books.close(); // Closing file
}

void bks::deleteBook()
{
	//TODO
}