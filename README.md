```
Gliwice, 14.01.2018
Semester: Winter, 2018/2019
Group: 3
Section: Tuesday, 8:30 A.M.
```

# Simple Library Database

Computer Programming Project

```
Author: Mateusz Urbanek
E-mail: ​mateurb418@polsl.pl
mateusz.urbanek.98@gmail.com
Tutor: PhD Michał Staniszewski
```

1. [Task topic](#task-topic)
2. [Project analysis](#project-analysis)
3. [External specification](#external-specification)
    - [1](#1)
    - [2](#2)
    - [3](#3)
    - [4](#4)
    - [5](#5)
    - [6](#6)
    - [7](#7)
    - [8](#8)
4. [Internal specification](#internal-specification)
   - [Classes.h](#classesh)
   - [Authors.h, Books.h, Genres.h](#authorsh-booksh-genresh)
   - [accWindow.h, accWindow.cpp, accWindow.resx](#accwindowh-accwindowcpp-accwindowresx)
   - [fileLocations.h](#filelocationsh)
   - [mainWindow.h, mainWindow.cpp, mainWindow.resx](#mainwindowh-mainwindowcpp-mainwindowresx)
   - [myConfig.h](#myconfigh)
   - [myCrypto.h](#mycryptoh)
5. [Source code](#source-code)
6. [Testing](#testing)
7. [Conclusions](#conclusions)
8. [References](#references)


## Task topic

Simple database for library - contains list of all books with their titles, genres, authors, creating new user, user system and adding new entries to every file.

## Project analysis

Project is using classes as nodes in queue to store data. Every new class is allocating memory for new part of data. Those classes are stored in a vector (```​std::vector<class>``` classList​). I have used classes stored inside vector, due to ease of acces to every single piece of data, ease of allocation of memory and ease of adding new pieces of data. Simple encryption is being done by ```​^=``` ​(bitwise XOR assignment) operator. The simplest explanation how it works is in table bellow:


| -       | Value | Binary representation |
|---------|-------|-----------------------|
| Input 1 | 5     | 00000101              |
| Input2  | 3     | 00000011              |
| Output  | 6     | 00000110              |

This isn’t the safest way to encrypt data, but it is simple and for first glance at encrypted data, its hard to distinguish what’s that. In my opinion it is harder to decode than Caesar cipher. The biggest problem I have encountered was the way, how data is stored after the program is turned off. Finally I have decided that the data will be stored inside ​```.dat​``` files. A file with the ​```.dat``` ​file extension is a generic data file that stores information specific to the application.


## External specification

#### 1.
![alt text](https://github.com/Sheerley/Simple-Library-Database/blob/master/1.png)

1.1. Login ​Textbox - here write your login/username, which you have created during account creation process (if you haven’t done it yet look at figure [2.](#2) and [3.](#3) ).
1.2. Key ​Textbox - here write your login/username, which you have obtained after account creation process (if you haven’t done it yet look at figure 2. and 3.).
1.3. Login ​Button - press it to get access to the library database.
1.4. Sign in ​Button - press it to create a new account.

#### 2.
![alt text](https://github.com/Sheerley/Simple-Library-Database/blob/master/2.png)

2.1. Username ​Textbox - provide your unique username and remember it.
2.2. Generate Password ​Button - press it, when you are sure, that you wrote proper username,​ there is no way to change it later! 
```diff
- Remember, that your key will be available at next run of the program!
```

#### 3.
![alt text](https://github.com/Sheerley/Simple-Library-Database/blob/master/3.png)

3.1. Key ​- here is your key/password. 
```diff
- Remember it, password recovery is impossible​!
```


#### 4.
![alt text](https://github.com/Sheerley/Simple-Library-Database/blob/master/4.png)

Warning informing about that you need to restart the program to use newly created account.


#### 5.
![alt text](https://github.com/Sheerley/Simple-Library-Database/blob/master/5.png)

5.1. Authors ​Button - click it to load the list of authors.
5.2. Books ​Button - click it to load the list of books.
5.3. Genres ​Button - click it to load the list of genres.
5.4. Previous ​Button - click to load previous object (ex. author, book or genre).
5.5. Next ​Button - click to load next object (ex. author, book or genre).
5.6. Add new entry ​Button - click to add new entry (ex. author, book or genre).


#### 6.
![alt text](https://github.com/Sheerley/Simple-Library-Database/blob/master/6.png)

6.1. ID ​Textbox - box containing ID of current entry (Ex. author, book or genre).
6.2. Name ​Textbox - box containing name of author, editable while adding new author,
```diff
- Disclaimer: ​ insert ID of author here while adding new book.
```
6.3. Surname ​Textbox - box containing surname of author, editable while adding new author.
6.4. Title ​Textbox - box containing title of book, editable while adding new book.
6.5. Genre ​ Textbox - box containing name of author, editable while adding new genre.
```diff
- Disclaimer: ​ insert ID of genre here while adding new book.
```
6.6. Add ​Button - Confirm, that data you inputed is correct.


#### 7.
![alt text](https://github.com/Sheerley/Simple-Library-Database/blob/master/7.png)

Information popup informing about that data was saved correctly.


#### 8.
![alt text](https://github.com/Sheerley/Simple-Library-Database/blob/master/8.png)

Warning informing about that you need to restart the program to load newly added data.

## Internal specification

### Classes.h

This file contains parent class for authors, books and genres classes.

### Authors.h, Books.h, Genres.h

This file contains namespaces and classes for authors, books and genres classes.
Namespaces contains:
- ```std::vector<★> arr_★```
    * List of all data.
- ```std::fstream f_★```
    * A filestream object in which are contained data of one type (Ex. books, genres
or authors).
- ```void​ ​read★​()```
    * Opens file with data, checks if it was opened properly. If not, then throws an exception. Else reads all contests of the file and pushes them to the ```​arr_★``` vector.
- ```void​ ​save★​()```
    * Saves the content of vector to file. 
    
star (★) means ​Books, Authors or Genres​.

Class ​ ```book``` ​contains:
- ```book​(​int​ _author_ID, std::string _title, ​int​ _genere_ID, ​int​ _book_ID)```
    * constructor of new book class;
- ```int​ author_ID```
- ```int​ book_ID```
- ```int​ genre_ID```
- ```std::string title```
Classes ​ ```author``` ​ and ​ ```genre``` ​are nearly the same as class ​```book```​. Only difference is what data is stored inside (names, surnames, etc.).

### accWindow.h, accWindow.cpp, accWindow.resx

This file contains definitions of account creation window, functions for every textbox, behavior, and button.
- ```System::Void ​Save_Click​(System::Object^ sender, System::EventArgs^ e)```
    * Behavior of Save button in account creation window:
        + if login provided in ​usernameTextBox ​is ​ ```admin```, ```ADMIN``` ​or ​ ```Admin``` ​throws an exception, because user can’t create administrator account.
        + else creates random generated password, uses ​```std::string encryption​(std::string str_to_encr, std::string key)```​ function to encrypt it and save to ​ azkudagamlah.dat ​file, then creates popup showing the key.

### fileLocations.h

This file contains a namespace, in which are included strings with location and name for each file.

1. ```authors.dat``` ​- this file contains all data connected with authors - how many authors
    are in the database, ID, name, and surname of each author.
2. ```books.dat``` ​- this file contains all data connected with books - how many books are in
    the database, ID, title, and ID of author and ID of the genre for each book.
3. ```genres.dat``` ​- this file contains all data connected with genres - how many genres are
    in the database, ID, and name of each genre.
4. ```azkudagamlah.dat``` ​- this file contains all data connected with accounts - how many
    accounts are in the database and encrypted combination of name/password/key.

### mainWindow.h, mainWindow.cpp, mainWindow.resx

This file contains definitions of main window, functions for every textbox, behavior, and button.

### myConfig.h

Basic configuration file storing some preprocessor directives.
- ```#​define​ ​_WIN32_WINNT​ ​0x```
    * Defines the minimal version of Windows on which program can be run, here 0x0500 ​is equal to Windows 2000.
- ```bool​ DEBUG = ​true```
    * Defines if the debug mode is active. Debug mode outputs values and information about functions and methods running.
- ```struct​ ​myException​ : ​public​ ​std​::exception```
    * Exception structure used inside my app. It inherites from ​std​::exception ​.
- ```C++
    #​ifdef​ _WIN
    #​define​ ​_CRT_SECURE_NO_WARNINGS
    #​endif
    ```  
    * Removes security warnings in the compiling phase.

### myCrypto.h

Contains ​namespace​ ​crpt​ which inside which is defined ​class​ ​Azk ​and functions operating on it.
- ```void​ ​readAzk​()```
    * Opens file with encrypted usernames, checks if it was opened properly. If not, then throws an exception. Else reads all contests of the file and pushes them to the ​arr_azk ​vector.
- ```std::string ​crpt::encryption​(std::string str_to_encr, std::string key)```
    * Functions used to perform bitwise XOR. It takes two strings, checks length of both, performs bitwise XOR on every character inside the string and then returns encrypted string ​return​ ```str_to_encr​```.

## Source code

Code can be found on GitHub:
- Private repository with access only for tutor:
    * [https://github.com/polsl-aei-cp3/6617aef5-gr31-repo/tree/master/Project/Sem3-2018.2019_MatUrb/Project](https://github.com/polsl-aei-cp3/6617aef5-gr31-repo/tree/master/Project/Sem3-2018.2019_MatUrb/Project)
- Public repository:
    * [https://github.com/Sheerley/Simple-Library-Database](https://github.com/Sheerley/Simple-Library-Database)

## Testing

All test were run on a Lenovo Yoga 720-15IKB running Windows 10, update 1809, version 10.0.17763, build 17763. It included several runs of a program in every combination while providing all types of data to text-spaces, and resulted in no errors. Wrong data provided to the program resulted in errors that were handled properly. This program is simple enough to allow me to check every possible combination.

## Conclusions

There is no option to delete or sort data from database. Those are the main things that should be concerned for future updates of that software.

## References

- [http://www.cplusplus.com/reference/iostream/](http://www.cplusplus.com/reference/iostream/)
- [http://www.cplusplus.com/reference/string/](http://www.cplusplus.com/reference/string/)
- [http://www.cplusplus.com/reference/fstream/](http://www.cplusplus.com/reference/fstream/)
- [http://www.cplusplus.com/reference/vector/](http://www.cplusplus.com/reference/vector/)
- [http://www.cplusplus.com/reference/sstream/](http://www.cplusplus.com/reference/sstream/)
- [http://www.cplusplus.com/reference/exception/](http://www.cplusplus.com/reference/exception/)
- [http://www.cplusplus.com/reference/chrono/](http://www.cplusplus.com/reference/chrono/)
- [https://en.wikipedia.org/wiki/Windows.h](https://en.wikipedia.org/wiki/Windows.h)
- [https://docs.microsoft.com/en-us/cpp/dotnet/overview-of-marshaling-in-cpp](https://docs.microsoft.com/en-us/cpp/dotnet/overview-of-marshaling-in-cpp)
- [https://en.cppreference.com/w/cpp/header%23C_compatibility_headers](https://en.cppreference.com/w/cpp/header%23C_compatibility_headers)
- [http://www.cplusplus.com/reference/cstdlib/](http://www.cplusplus.com/reference/cstdlib/)
- [http://www.cplusplus.com/reference/ctime/](http://www.cplusplus.com/reference/ctime/)